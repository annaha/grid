var video;

var vScale = 16;

function setup() {
  createCanvas(window.innerWidth, window.innerHeight);
  pixelDensity(1);
  video = createCapture(VIDEO);
  video.size(width/vScale, height/vScale);
  video.hide()
}

function draw() {
  background(51);

  video.loadPixels();
  loadPixels();
    for (var x = 0; x < video.width; x++) {
    for (var y = 0; y < video.height; y++) {
      var index = (video.width - x + 1 + (y * video.width))*4;
      var r = video.pixels[index+2];
      var g = video.pixels[index+1];
      var b = video.pixels[index+0];

      var bright = (r+g+b)/3;

      // var w = map(bright, 0, 255, 0, vScale);
      var w = map(bright, 255, 0, 0, vScale); 
      // invert

      noStroke();
      fill(255);
      rectMode(CENTER);
      rect(x*vScale, y*vScale, w, w);

    }
  }

}

 function keyPressed(){
  if(keyCode === 32){
    saveCanvas(canvas, 'intercam', '.png')
  }
 }
